<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'password_valid' => 'Mật khẩu yêu cầu phải tồn tại 8 ký tự bao gồm viết thường, viết hoa, ký tự đặt biệt và số',
    'data_valid'     => 'Dữ liệu được chỉ định không hợp lệ.',
    'token_valid'    => 'Token đặt lại mật khẩu này không hợp lệ.',

    'required' => 'Trường :attribute không được bỏ trống.',
    'unique' => 'Trường :attribute đã có trong cơ sở dữ liệu.',
];

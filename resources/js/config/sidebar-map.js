module.exports = [
  {
    name: 'Dashboard',
    icon: 'dashboard',
    route: 'dashboard.index',
    access: 'admin',
  },
  // ===== #{{ Videos }} ===== //
  {
    name: 'Videos',
    icon: 'catalog-fill',
    route: 'dashboard.index',
    access: 'common',
  },
  {
    name: 'Contestants',
    icon: 'catalog-fill',
    route: 'dashboard.index',
    access: 'common',
  },
  {
    name: 'Viewers',
    icon: 'catalog-fill',
    route: 'dashboard.index',
    access: 'common',
  },
  {
    name: 'Categories',
    icon: 'catalog-fill',
    route: 'dashboard.index',
    access: 'common',
  },
  {
    name: 'Setting',
    icon: 'catalog-fill',
    route: 'dashboard.index',
    access: 'common',
  },
  {
    name: 'Site Management',
    icon: 'catalog-fill',
    route: 'dashboard.index',
    access: 'common',
  },
];

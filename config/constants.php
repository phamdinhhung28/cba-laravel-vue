<?php

return [
    /*
     |-------------------------------------------------
     | Define constant for Decentralization
     |-------------------------------------------------
     */
    'module_access' => [
        1 => 'Super Admin',
        2 => 'Platform',
        3 => 'Merchant',
        4 => 'Common'
    ],

    /*
     |-------------------------------------------------
     | Define constant for Pagination
     |-------------------------------------------------
     */
    'pagination' => [
        'limit' => 20
    ]
];
